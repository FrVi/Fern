# Barnsley fern

<br>
<img src="https://gitlab.com/FrVi/Fern/raw/master/images/fern.png" alt="Drawing" width="406"/>
<br>

<br>
## Principle

The Barnsley fern is a fractal object (iterated function system) built by recursive applications of similarities on a pattern.

<br>
## The Program

The program is based on SDL and allows modification of the fractal by manipulating the pattern in the upper left frame.<br>
<br>
I made this in 2008 while in high school. 
