#include <stdlib.h>
#include <stdio.h> 
#include <SDL/SDL.h> 
#include <math.h>
#include "fougere.h"
#include <SDL/SDL_gfxPrimitives.h> 
#include <SDL/SDL_gfxPrimitives_font.h> 
#include <string.h>

void pause()
{
	int continuer = 1;
	SDL_Event event;

	while (continuer)
	{
		SDL_WaitEvent(&event);
		switch (event.type)
		{
		case SDL_QUIT:
			continuer = 0;
		}
	}
}

void fougereBelle(Coordonnees a, Coordonnees b, float sens, SDL_Event *event)
{
	/*
	C
	F    /
	\  /
	A
	|
	D - E
	|
	B
	*/

	if ((*event).type != SDL_QUIT && (*event).type != SDL_KEYDOWN && (*event).type != SDL_MOUSEBUTTONDOWN)
	{
		SDL_PollEvent(event);

		Coordonnees c, d, e, f, ab, ad, db;

		ab.x = b.x - a.x;
		ab.y = b.y - a.y;

		float L = ab.x*ab.x + ab.y*ab.y;
		if (L > 1000)
		{
			SDL_Flip(ecran);
		}

		if (L > epsilon2)
		{
			d.x = (a.x + b.x) / 2;
			d.y = (a.y + b.y) / 2;

			c.x = a.x + r1*(cosBAC * ab.x - sens * sinBAC * ab.y);
			c.y = a.y + r1*(sens * sinBAC * ab.x + cosBAC * ab.y);

			ad.x = d.x - a.x;
			ad.y = d.y - a.y;
			f.x = a.x + r3*(cosBAF * ad.x - sens * sinBAF * ad.y);
			f.y = a.y + r3*(sens * sinBAF * ad.x + cosBAF * ad.y);

			db.x = b.x - d.x;
			db.y = b.y - d.y;
			e.x = d.x + r2*(cosBDE * db.x - sens * sinBDE * db.y);
			e.y = d.y + r2*(sens * sinBDE * db.x + cosBDE * db.y);

			int grad = pow(L, 0.4) * 35;
			grad = grad > 255 ? 255 : grad;

			lineRGBA(ecran, d.x, d.y, e.x, e.y, grad, 255 - grad / 6, 0, grad);
			lineRGBA(ecran, a.x, a.y, f.x, f.y, grad, 255 - grad / 6, 0, grad);
			lineRGBA(ecran, a.x, a.y, c.x, c.y, grad, 255 - grad / 6, 0, grad);

			fougereBelle(f, a, sens, event);
			fougereBelle(e, d, -sens, event);
			fougereBelle(c, a, sens, event);

			/*SDL_Flip(ecran);*/
		}
	}
}

void fougereRapide(Coordonnees a, Coordonnees b, float sens)
{
	/*
	C
	F    /
	\  /
	A
	|
	D - E
	|
	B
	*/

	Coordonnees c, d, e, f, ab, ad, db;

	ab.x = b.x - a.x;
	ab.y = b.y - a.y;

	float L = ab.x*ab.x + ab.y*ab.y;

	if (L > epsilon)
	{
		d.x = (a.x + b.x) / 2;
		d.y = (a.y + b.y) / 2;

		c.x = a.x + r1*(cosBAC * ab.x - sens * sinBAC * ab.y);
		c.y = a.y + r1*(sens * sinBAC * ab.x + cosBAC * ab.y);

		ad.x = d.x - a.x;
		ad.y = d.y - a.y;
		f.x = a.x + r3*(cosBAF * ad.x - sens * sinBAF * ad.y);
		f.y = a.y + r3*(sens * sinBAF * ad.x + cosBAF * ad.y);

		db.x = b.x - d.x;
		db.y = b.y - d.y;
		e.x = d.x + r2*(cosBDE * db.x - sens * sinBDE * db.y);
		e.y = d.y + r2*(sens * sinBDE * db.x + cosBDE * db.y);

		lineRGBA(ecran, d.x, d.y, e.x, e.y, 255, 255, 0, 255);
		lineRGBA(ecran, a.x, a.y, f.x, f.y, 255, 255, 0, 255);
		lineRGBA(ecran, a.x, a.y, c.x, c.y, 255, 255, 0, 255);

		fougereRapide(f, a, sens);
		fougereRapide(e, d, -sens);
		fougereRapide(c, a, sens);

		/*SDL_Flip(ecran);*/
	}
}

float norme(SDL_Rect* A, SDL_Rect* B)
{
	SDL_Rect  vBA = { (A->x - B->x), (A->y - B->y) };
	float distance = sqrt(vBA.x * vBA.x + vBA.y * vBA.y);

	return distance;
}

void deplacerPointInitial(SDL_Rect* A, SDL_Rect* B, SDL_Rect* C, SDL_Rect* D, SDL_Rect* E, SDL_Rect* F,
	int dmin, SDL_Event event, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position, int *point, int *cond)
{
	if (*cond == 1)
	{
		SDL_Rect Event = { event.button.x, event.button.y };
		float distance = norme(A, &Event);
		float distance2 = norme(B, &Event);
		float distance3 = norme(C, &Event);
		float distance4 = norme(E, &Event);
		float distance5 = norme(F, &Event);

		if (distance <= dmin)
		{
			A->x = event.button.x;
			A->y = event.button.y;

			fougere0(A, B, C, D, E, F, ecran, rectangle, position);

			*point = 1;
		}
		else if (distance2 <= dmin)
		{
			B->x = event.button.x;
			B->y = event.button.y;

			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
			*point = 2;
		}
		else if (distance3 <= dmin)      
		{
			C->x = event.button.x;
			C->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
			*point = 3;
		}
		else if (distance4 <= dmin)      
		{
			E->x = event.button.x;
			E->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
			*point = 4;
		}
		else if (distance5 <= dmin)
		{
			F->x = event.button.x;
			F->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
			*point = 5;

		}
	}

	*cond = 0;
}

void fougere0(SDL_Rect* A, SDL_Rect* B, SDL_Rect* C, SDL_Rect* D, SDL_Rect* E, SDL_Rect* F, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position)
{
	SDL_BlitSurface(rectangle, &position, ecran, &position);

	Cadre(A); Cadre(B); Cadre(C);
	Cadre(E); Cadre(F);
	D->x = (A->x + B->x) / 2;
	D->y = (A->y + B->y) / 2;

	lineColor(ecran, A->x, A->y, B->x, B->y, 0xFF0000FF);
	lineColor(ecran, A->x, A->y, F->x, F->y, 0x00FF00FF);
	lineColor(ecran, A->x, A->y, C->x, C->y, 0x0000FFFF);
	lineColor(ecran, D->x, D->y, E->x, E->y, 0xFFFF00FF);
	SDL_Flip(ecran);
}

void Cadre(SDL_Rect* A)
{
	A->x = A->x > 200 ? 200 : A->x;
	A->y = A->y > 200 ? 200 : A->y;
}

void deplacerPoint(SDL_Rect* A, SDL_Rect* B, SDL_Rect* C, SDL_Rect* D, SDL_Rect* E, SDL_Rect* F,
	int dmin, SDL_Event event, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position, int *point, int *cond)
{
	if (*cond == 0)
	{
		if (*point == 1)
		{
			A->x = event.button.x;
			A->y = event.button.y;

			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
		}
		else if (*point == 2)
		{
			B->x = event.button.x;
			B->y = event.button.y;

			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
		}
		else if (*point == 3)
		{
			C->x = event.button.x;
			C->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
		}
		else if (*point == 4)
		{
			E->x = event.button.x;
			E->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
		}
		else if (*point == 5)
		{

			F->x = event.button.x;
			F->y = event.button.y;
			fougere0(A, B, C, D, E, F, ecran, rectangle, position);
		}
	}
}
