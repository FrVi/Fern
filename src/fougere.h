#ifndef FOUGERE_H
#define FOUGERE_H

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <math.h>

#define xtaillereel 900
#define ytaillereel 668

#define epsilon 1
#define epsilon2 0.1
/*
     C
F   /
 \ /
  A
  |
  D - E
  |
  B
*/

#define  BAC  3.00149238898
#define  BDE  1.67387778343
#define  BAF  4.48815457469

float r3;
float r2;
float  r1;

float cosBAC;
float sinBAC;

float cosBDE;
float sinBDE;

float cosBAF;
float sinBAF;

SDL_Surface* ecran;

typedef struct Coordonnees Coordonnees;
struct Coordonnees
{
	float x;
	float y;
};

void fougereRapide(Coordonnees a, Coordonnees b, float sens);
void fougereBelle(Coordonnees a, Coordonnees b, float sens, SDL_Event *event);

void fougere0(SDL_Rect* A,SDL_Rect* B,SDL_Rect* C,SDL_Rect* D,SDL_Rect* E, SDL_Rect* F, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position);
void Cadre(SDL_Rect* A);
void deplacerPointInitial(SDL_Rect* A, SDL_Rect* B, SDL_Rect* C, SDL_Rect* D, SDL_Rect* E, SDL_Rect* F, int dmin, SDL_Event event, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position, int *point,int *cond);
void deplacerPoint(SDL_Rect* A, SDL_Rect* B, SDL_Rect* C, SDL_Rect* D, SDL_Rect* E, SDL_Rect* F, int dmin, SDL_Event event, SDL_Surface *ecran, SDL_Surface *rectangle, SDL_Rect position, int *point,int *cond); 
 
float norme(SDL_Rect* A, SDL_Rect* B);
void pause();

#endif//FOUGERE_H
