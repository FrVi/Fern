#include <stdlib.h> 
#include <stdio.h> 
#include <SDL/SDL.h> 
#include <math.h> 
#include "fougere.h"
#include <SDL/SDL_gfxPrimitives.h> 
#include <SDL/SDL_gfxPrimitives_font.h> 
#include <string.h>

int main(int argc, char *argv[])
{
	float rmax = 0.9;

	int point = 1;
	int cond = 1;

	SDL_Rect  AB, AC, DB, DE, AF;
	float  normAB, normDB, normDE, normAC, normAF;
	r1 = 0.837998;
	r2 = 0.620314;
	r3 = 0.621954;

	int dmin = 10;

	cosBAC = cos(BAC);
	sinBAC = sin(-BAC);

	cosBDE = cos(BDE);
	sinBDE = sin(-BDE);

	cosBAF = cos(BAF);
	sinBAF = sin(-BAF);

	SDL_Init(SDL_INIT_VIDEO);
	ecran = SDL_SetVideoMode(xtaillereel, ytaillereel, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("Fougere(v1.00) - Frederic Vitzikam ", NULL);
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));

	SDL_Surface *rectangle = NULL;
	rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, xtaillereel, ytaillereel, 32, 0, 0, 0, 0);
	SDL_Rect position = { 0, 0, 201, 201 };

	SDL_Rect A = { 99, 98 };
	SDL_Rect B = { 99, 192 };
	SDL_Rect C = { 110, 20 };
	SDL_Rect D = { (A.x + B.x) / 2, (A.y + B.y) / 2 };
	SDL_Rect E = { 128, 142 };
	SDL_Rect F = { 42, 85 };

	SDL_Event event;

	Coordonnees a = { A.x + 251, A.y + 404 },
		        b = { B.x + 251, B.y + 404 };

	SDL_BlitSurface(ecran, &position, rectangle, &position);
	lineColor(ecran, A.x, A.y, B.x, B.y, 0xFF0000FF);
	lineColor(ecran, A.x, A.y, F.x, F.y, 0x00FF00FF);
	lineColor(ecran, A.x, A.y, C.x, C.y, 0x0000FFFF);
	lineColor(ecran, D.x, D.y, E.x, E.y, 0xFFFF00FF);

	rectangleColor(ecran, -1, -1, 201, 201, 0xFFFF00FF);

	lineColor(ecran, a.x, a.y, b.x, b.y, 0xFFFF00FF);

	fougereBelle(a, b, 1, &event);

	SDL_Flip(ecran);

	while (event.type != SDL_QUIT)
	{
		SDL_WaitEvent(&event);

		if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			while (event.type != SDL_MOUSEBUTTONUP && event.type != SDL_QUIT)
			{
				SDL_WaitEvent(&event);

				deplacerPoint(&A, &B, &C, &D, &E, &F, dmin, event, ecran, rectangle, position, &point, &cond);
				deplacerPointInitial(&A, &B, &C, &D, &E, &F, dmin, event, ecran, rectangle, position, &point, &cond);

				AB.x = B.x - A.x;
				AB.y = B.y - A.y;
				AC.x = C.x - A.x;
				AC.y = C.y - A.y;
				normAB = norme(&A, &B);
				normAC = norme(&A, &C);

				DB.x = B.x - D.x;
				DB.y = B.y - D.y;
				DE.x = E.x - D.x;
				DE.y = E.y - D.y;
				normDB = norme(&D, &B);
				normDE = norme(&D, &E);

				AF.x = F.x - A.x;
				AF.y = F.y - A.y;
				normAF = norme(&A, &F);

				cosBAF = (AB.x *AF.x + AB.y *AF.y) / (normAF *normAB);
				sinBAF = (AB.x *AF.y - AB.y *AF.x) / (normAF *normAB);
				r3 = normAF / normAB;

				cosBDE = (DB.x *DE.x + DB.y *DE.y) / (normDE *normDB);
				sinBDE = (DB.x *DE.y - DB.y *DE.x) / (normDE *normDB);
				r2 = normDE / normDB;

				cosBAC = (AB.x *AC.x + AB.y *AC.y) / (normAB *normAC);
				sinBAC = (AB.x *AC.y - AB.y *AC.x) / (normAB *normAC);
				r1 = normAC / normAB;

				if (r1 > rmax) r1 = rmax;
				if (r2 > rmax) r2 = rmax;
				if (r3 > rmax) r3 = rmax;

				a.x = A.x + 251;
				a.y = A.y + 404;

				b.x = B.x + 251;
				b.y = B.y + 404;

				boxColor(ecran, 202, 0, xtaillereel, ytaillereel, 0x000000FF);
				boxColor(ecran, 0, 202, 202, ytaillereel, 0x000000FF);

				lineColor(ecran, a.x, a.y, b.x, b.y, 0xFFFF00FF);
				fougereRapide(a, b, 1);

				SDL_BlitSurface(rectangle, &position, ecran, &position);

				lineColor(ecran, A.x, A.y, B.x, B.y, 0xFF0000FF);
				lineColor(ecran, A.x, A.y, F.x, F.y, 0x00FF00FF);
				lineColor(ecran, A.x, A.y, C.x, C.y, 0x0000FFFF);
				lineColor(ecran, D.x, D.y, E.x, E.y, 0xFFFF00FF);

				SDL_Flip(ecran);
			}
			cond = 1;

			if (event.type != SDL_QUIT)
			{
				boxColor(ecran, 202, 0, xtaillereel, ytaillereel, 0x000000FF);
				boxColor(ecran, 0, 202, 202, ytaillereel, 0x000000FF);
				lineColor(ecran, a.x, a.y, b.x, b.y, 0xFFFF00FF);
				event.type = SDL_MOUSEMOTION;
				fougereBelle(a, b, 1, &event);
				SDL_BlitSurface(rectangle, &position, ecran, &position);
				lineColor(ecran, A.x, A.y, B.x, B.y, 0xFF0000FF);
				lineColor(ecran, A.x, A.y, F.x, F.y, 0x00FF00FF);
				lineColor(ecran, A.x, A.y, C.x, C.y, 0x0000FFFF);
				lineColor(ecran, D.x, D.y, E.x, E.y, 0xFFFF00FF);
				SDL_Flip(ecran);
			}
		}
	}

	SDL_FreeSurface(rectangle);
	SDL_Quit();

	return EXIT_SUCCESS;
}
